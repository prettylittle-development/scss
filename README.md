# SCSS
A collection of mixins to be used within SCSS projects.

## Mixins

### fluid.scss
Allows you to create typography that scales **from** and **to** values based on the browser width. Search for "css locks".
